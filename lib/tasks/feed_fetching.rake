require "#{Rails.root}/app/helpers/links_helper"
include LinksHelper

desc 'Get feeds'

task  :get_pinboard_feed => :environment do
  feed = Feedzirra::Feed.fetch_and_parse("http://feeds.pinboard.in/rss/popular/")
  feed.entries.each do |e|
    next if e.url =~ /archiveofourown/
    Link.where(:url => e.url).first_or_create(
                                              :title => e.title, 
                                              :contributor => e.author, 
                                              :last_seen_on => Time.zone.today, 
                                              :date_added => e.published, 
                                              :source => :pinboard
                                              )
    picplz_url e.url
  end
end

task  :get_hnews_feed => :environment do
  feed = Feedzirra::Feed.fetch_and_parse("http://news.ycombinator.com/rss")
  feed.entries.each do |e|
    Link.where(:url => e.url).first_or_create(
                                              :title => e.title, 
                                              :last_seen_on => Time.zone.today, 
                                              :date_added => Time.zone.today, 
                                              :source => :hnews,
                                              :summary => URI.extract(e.summary)[0]
                                              )
    picplz_url e.url
  end
end

  task  :get_my_pinboard_feed => :environment do
  feed = Feedzirra::Feed.fetch_and_parse("http://feeds.pinboard.in/rss/secret:f45a53bd3812304e061a/u:heliostatic/")
  feed.entries.reverse.each do |e|
    Link.where(:url => e.url).first_or_create(
                                              :title => e.title, 
                                              :last_seen_on => Time.zone.today, 
                                              :date_added => e.published, 
                                              :source => :pinboard_heliostatic,
                                              :contributor => e.author
                                              )
    picplz_url e.url
  end
end