desc 'Maintain records'
task :delete_old_records => :environment do
  Link.destroy_all(['updated_at < ?', 30.days.ago])
end