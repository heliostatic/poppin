class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.text :url
      t.datetime :date_added
      t.text :title

      t.timestamps
    end
  end
end
