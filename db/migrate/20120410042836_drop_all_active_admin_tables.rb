class DropAllActiveAdminTables < ActiveRecord::Migration
  def self.up
    drop_table :active_admin_comments
    drop_table :admin_users
  end

  def self.down
    raise IrreversibleMigration
  end
end
