class AddSourceAndSummarytoLink < ActiveRecord::Migration
  def change
    add_column :links, :source, :string
    add_column :links, :summary, :string
  end
end
