class AddLastSeenOntoLinks < ActiveRecord::Migration
  def change
    add_column :links, :last_seen_on, :date
  end
end
