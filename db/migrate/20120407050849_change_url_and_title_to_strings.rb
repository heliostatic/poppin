class ChangeUrlAndTitleToStrings < ActiveRecord::Migration
  def change
    change_column :links, :url, :string
    change_column :links, :title, :string
  end
end
