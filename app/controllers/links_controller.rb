class LinksController < ApplicationController
respond_to :html, :xml, :js

  def index
    @links = Link.page params[:page]
    respond_with @links
  end

  # def show
  # 	@link = Link.find(params[:id])
  # end
end
