class HomeController < ApplicationController
  def index
    @links = Link.limit(100).page params[:page]
  end
end
