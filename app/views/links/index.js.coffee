$("ul.thumbnails").append "<%= escape_javascript(render(:partial => 'link', :collection => @links)) %>"
$("#loading").replaceWith("<div id='loading'><%= escape_javascript(
    link_to_next_page @links, 'Load more',
                        :remote => true,
                        :class => 'btn more_link', 
                        :'data-loading-text' => 'Loading...')
                          %></div>");