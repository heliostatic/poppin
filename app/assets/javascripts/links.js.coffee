# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  if $('.more_link').length
    $(window).scroll ->
      url = $('.more_link').attr('href')
      if url && $(window).scrollTop() > $(document).height() - $(window).height() - 50
        $('#loading').text("Fetching more links...")
        $.getScript(url)