class Link < ActiveRecord::Base
attr_accessible :date_added, :title, :url, :contributor, :last_seen_on, :summary, :source

  validates :url, :title, :presence => true
  validates :url, :uniqueness => true
  paginates_per 16
  
  default_scope :order => 'created_at DESC'
  scope :created_on, lambda {|date| {:conditions => ['created_at >= ? AND created_at <= ?', date.beginning_of_day, date.end_of_day]}}
  scope :recent, -> { where(:last_seen_on => Time.zone.today) }
  
  def self.today
    self.created_on(Time.zone.today)
  end
  
  def pinboard_user_name
    "http://pinboard.in/u:#{contributor}" if source.starts_with?("pinboard")
  end
  
  def host
    host = URI.parse(url)
    host.host
  end
  
end
# == Schema Information
#
# Table name: links
#
#  id           :integer         not null, primary key
#  url          :string(255)
#  date_added   :datetime
#  title        :string(255)
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  contributor  :string(255)
#  last_seen_on :date
#  source       :string(255)
#  summary      :string(255)
#

