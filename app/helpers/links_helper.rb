module LinksHelper
  def picplz_image_tag url, options = {}
    
    # parse size
    dim = self.parse(options)

    # ensure image alt 
    alt = options.key?(:alt) ? options.delete(:alt) : url
    
    # build image tag
    img =  '<img'
    img << " src='#{ picplz_url(url) }'"
    img << " alt='#{ alt }'"
    img << " width='#{ dim[:width] }'" if options[:size]
    img << " height='#{ dim[:height] }'" if options[:size]
    options.each_pair do |k, v|
      img << " #{ k }=#{ v }" unless v.nil? || v == ''
    end
    img << ' />'
    img.respond_to?(:html_safe) ? img.html_safe : img # Utilize ActiveSupport if loaded
  end

   
  def parse options
  
    # distill image size
    size   = (options[:size]  || '1024x800').split('x')
    width  = options[:width]  || size[0]
    height = options[:height] || size[1]
    
    # return dimensions hash
    {
      :size   => "#{ width }x#{ height }",
      :width  => width,
      :height => height
    }
  end

  def picplz_url url, size = '1024x800'
    "http://imagemaker.bencohen.net/#{size}/#{url}"
  end
end
