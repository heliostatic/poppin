PinboardPopular::Application.routes.draw do
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  devise_for :users

  root :to => "links#index"
  resources :links do
    get 'page/:page', :action => :index, :on => :collection
  end
end
